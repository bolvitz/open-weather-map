package com.bolvitz.openweathermap.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.bolvitz.openweathermap.OpenWeatherMap;
import com.bolvitz.openweathermap.R;
import com.bolvitz.openweathermap.di.component.ActivityComponent;
import com.bolvitz.openweathermap.di.component.DaggerActivityComponent;

public abstract class BaseActivity extends AppCompatActivity {


    protected abstract void injectComponent(ActivityComponent component);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(DaggerActivityComponent.builder().appComponent(getApp().getAppComponent()).build());

    }

    protected OpenWeatherMap getApp() {
        return (OpenWeatherMap) this.getApplicationContext();
    }

    public void launchNextPage(Context packageContext, Class<?> cls, boolean isMainPage, Bundle extras) {
        Intent i = new Intent();
        i.setClass(packageContext, cls);
        if (extras != null)
            i.putExtras(extras);
        if (isMainPage)
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        if (isMainPage)
            finish();
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }
}
