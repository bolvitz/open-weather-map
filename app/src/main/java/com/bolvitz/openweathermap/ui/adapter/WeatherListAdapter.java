package com.bolvitz.openweathermap.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bolvitz.openweathermap.R;
import com.bolvitz.openweathermap.controller.Callback;
import com.bolvitz.openweathermap.model.Entity;
import com.bolvitz.openweathermap.model.Weather;
import com.bolvitz.openweathermap.utils.sort.EntityCompare;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.ViewHolder> {

    private List<Entity> weatherList;
    private Context context;
    private Callback.OnItemClick callback;

    public WeatherListAdapter(Context context, List<Entity> weatherList) {
        this.context = context;
        this.weatherList = weatherList;
    }

    public void updateAdapter(List<Entity> entities) {
        this.weatherList = entities;
        notifyDataSetChanged();
    }

    public void setCallback(Callback.OnItemClick callback) {
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        DecimalFormat df = new DecimalFormat("#.##");
        Entity entity = weatherList.get(position);

        List<Weather> weatherList = Arrays.asList(entity.weather);
        Collections.sort(weatherList, new EntityCompare());
        Weather weather = weatherList.get(0);

        holder.tvWeatherLocation.setText(String.format("%s, %s", entity.name, entity.sys.country));
        holder.tvWeatherDescription.setText(weather.description);
        holder.tvWeatherDetails.setText(String.format(Locale.getDefault(), "temperature from %s to %s °С", df.format(entity.main.temp_min - 273.15), df.format(entity.main.temp_max - 273.15)));
        holder.tvWeatherTemp.setText(String.format(Locale.getDefault(), "%s °С", df.format(entity.main.temp - 273.15)));
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvWeatherLocation)
        TextView tvWeatherLocation;
        @BindView(R.id.tvWeatherDescription)
        TextView tvWeatherDescription;
        @BindView(R.id.tvWeatherDetails)
        TextView tvWeatherDetails;
        @BindView(R.id.tvWeatherTemp)
        TextView tvWeatherTemp;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> {
                int position = getLayoutPosition();
                if (callback != null && position != RecyclerView.NO_POSITION) {
                    Entity entity = weatherList.get(position);
                    callback.onItemClick(entity, position);
                }
            });
        }
    }

}