package com.bolvitz.openweathermap.ui.activity.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bolvitz.openweathermap.R;
import com.bolvitz.openweathermap.controller.ApiController;
import com.bolvitz.openweathermap.di.component.ActivityComponent;
import com.bolvitz.openweathermap.model.Entity;
import com.bolvitz.openweathermap.model.Weather;
import com.bolvitz.openweathermap.ui.activity.BaseActivity;
import com.bolvitz.openweathermap.utils.sort.EntityCompare;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bolvitz.openweathermap.database.Constants.WEATHER_BUNDLE;

public class DetailsActivity extends BaseActivity implements DetailsActivityView {

    @Inject
    ApiController apiController;

    Entity selectedEntity;
    @BindView(R.id.tvDetailsTemp)
    TextView tvDetailsTemp;
    @BindView(R.id.ivDetailsIcon)
    ImageView ivDetailsIcon;
    @BindView(R.id.tvDetailsLocation)
    TextView tvDetailsLocation;
    @BindView(R.id.tvDetailsDescription)
    TextView tvDetailsDescription;
    @BindView(R.id.tvDetails)
    TextView tvDetails;

    private DetailsActivityPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.selectedEntity = new Gson().fromJson((String) bundle.get(WEATHER_BUNDLE), Entity.class);

            updateIU(this.selectedEntity);

        }

        this.presenter = new DetailsActivityPresenter(this, apiController);
    }

    @Override
    protected void injectComponent(ActivityComponent component) {
        component.inject(this);
    }

    @Override
    public void setWeather(Entity weather) {
        this.selectedEntity = weather;
        updateIU(this.selectedEntity);
    }

    @Override
    public void setError() {
        Toast.makeText(this, "An error occurred", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnDetailsRefresh)
    public void onViewClicked() {
        this.presenter.getSelectedWeather(this.selectedEntity.id);
    }

    private void updateIU(Entity selectedEntity) {

        DecimalFormat df = new DecimalFormat("#.##");

        List<Weather> weatherList = Arrays.asList(selectedEntity.weather);
        Collections.sort(weatherList, new EntityCompare());
        Weather weather = weatherList.get(0);

        tvDetailsTemp.setText(String.format(Locale.getDefault(), "%s °С", df.format(selectedEntity.main.temp - 273.15)));
        tvDetailsLocation.setText(String.format("%s, %s", selectedEntity.name, selectedEntity.sys.country));
        tvDetailsDescription.setText(weather.description);
        tvDetails.setText(String.format(Locale.getDefault(), "temperature from %s to %s °С", df.format(selectedEntity.main.temp_min - 273.15), df.format(selectedEntity.main.temp_max - 273.15)));

        Glide.with(this).load(String.format("http://openweathermap.org/img/w/%s.png", weather.icon)).into(ivDetailsIcon);

    }
}
