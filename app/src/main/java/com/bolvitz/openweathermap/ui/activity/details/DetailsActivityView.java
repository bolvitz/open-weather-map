package com.bolvitz.openweathermap.ui.activity.details;

import com.bolvitz.openweathermap.model.Entity;

interface DetailsActivityView {

    void setWeather(Entity weather);

    void setError();

}
