package com.bolvitz.openweathermap.ui.activity.main;

import android.os.Bundle;

import com.bolvitz.openweathermap.R;
import com.bolvitz.openweathermap.di.component.ActivityComponent;
import com.bolvitz.openweathermap.ui.activity.BaseActivity;
import com.bolvitz.openweathermap.ui.fragment.main.WeatherFragment;

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        replaceFragment(WeatherFragment.newInstance());

    }

    @Override
    protected void injectComponent(ActivityComponent component) {
        component.inject(this);
    }


}
