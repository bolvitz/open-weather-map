package com.bolvitz.openweathermap.ui.activity.details;

import com.bolvitz.openweathermap.controller.ApiController;
import com.bolvitz.openweathermap.controller.Callback;
import com.bolvitz.openweathermap.model.Entity;
import com.bolvitz.openweathermap.model.MultiEntityResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class DetailsActivityPresenter {

    private DetailsActivityView view;
    private ApiController apiController;

    DetailsActivityPresenter(DetailsActivityView view, ApiController apiController) {
        this.view = view;
        this.apiController = apiController;
    }

    void getSelectedWeather(int id) {
        apiController.getWeatherById(id, new Callback.MultiEntityListener() {
            @Override
            public void onSuccess(Object object) {
                MultiEntityResponse response = (MultiEntityResponse) object;
                if (response != null) {
                    List<Entity> entities = new ArrayList<>(Arrays.asList(response.list));
                    if (entities.size() > 0) {
                        view.setWeather(entities.get(0));
                    }
                }
            }

            @Override
            public void onError() {
                view.setError();
            }
        });

    }
}
