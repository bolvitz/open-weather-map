package com.bolvitz.openweathermap.ui.activity.splash;

import android.os.Bundle;
import android.os.Handler;

import com.bolvitz.openweathermap.R;
import com.bolvitz.openweathermap.di.component.ActivityComponent;
import com.bolvitz.openweathermap.ui.activity.BaseActivity;
import com.bolvitz.openweathermap.ui.activity.main.MainActivity;

public class SplashActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashDelay();
    }

    @Override
    protected void injectComponent(ActivityComponent component) {
        component.inject(this);
    }

    private void splashDelay() {
        Handler handler = new Handler();
        handler.postDelayed(() -> launchNextPage(SplashActivity.this, MainActivity.class, true, null), 2000);
    }
}
