package com.bolvitz.openweathermap.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.bolvitz.openweathermap.OpenWeatherMap;
import com.bolvitz.openweathermap.di.component.DaggerFragmentComponent;
import com.bolvitz.openweathermap.di.component.FragmentComponent;
import com.bolvitz.openweathermap.ui.activity.BaseActivity;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectComponent(DaggerFragmentComponent.builder().appComponent(getApp().getAppComponent()).build());
    }

    protected OpenWeatherMap getApp() {
        return (OpenWeatherMap) getActivity().getApplicationContext();
    }

    protected BaseActivity getParentActivity() {
        return (BaseActivity) getActivity();
    }

    protected abstract void injectComponent(FragmentComponent component);

}
