package com.bolvitz.openweathermap.ui.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bolvitz.openweathermap.R;
import com.bolvitz.openweathermap.controller.ApiController;
import com.bolvitz.openweathermap.database.Constants;
import com.bolvitz.openweathermap.di.component.FragmentComponent;
import com.bolvitz.openweathermap.model.Entity;
import com.bolvitz.openweathermap.ui.activity.details.DetailsActivity;
import com.bolvitz.openweathermap.ui.adapter.WeatherListAdapter;
import com.bolvitz.openweathermap.ui.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class WeatherFragment extends BaseFragment implements WeatherFragmentView {

    @Inject
    ApiController apiController;

    @BindView(R.id.rvWeather)
    RecyclerView rvWeather;

    private Unbinder unbinder;
    private WeatherFragmentPresenter presenter;
    private WeatherListAdapter weatherAdapter;

    public WeatherFragment() {
    }

    public static WeatherFragment newInstance() {
        return new WeatherFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, view);

        this.presenter = new WeatherFragmentPresenter(this, apiController);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupAdapter();
        this.presenter.getWeathers();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    protected void injectComponent(FragmentComponent component) {
        component.inject(this);
    }

    @Override
    public void setWeathers(List<Entity> weathers) {
        weatherAdapter.updateAdapter(weathers);
    }

    @Override
    public void setError() {
        Toast.makeText(getParentActivity(), "An error occurred", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnRefresh)
    public void onViewClicked() {
        this.presenter.getWeathers();
    }

    private void setupAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getParentActivity(), LinearLayoutManager.VERTICAL, false);
        rvWeather.setLayoutManager(linearLayoutManager);
        weatherAdapter = new WeatherListAdapter(getParentActivity(), new ArrayList<>());
        rvWeather.setAdapter(weatherAdapter);

        weatherAdapter.setCallback((entity, position) -> {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.WEATHER_BUNDLE, entity.toString());
            getParentActivity().launchNextPage(getParentActivity(), DetailsActivity.class, false, bundle);
        });

    }
}
