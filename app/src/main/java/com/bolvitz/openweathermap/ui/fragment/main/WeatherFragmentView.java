package com.bolvitz.openweathermap.ui.fragment.main;

import com.bolvitz.openweathermap.model.Entity;

import java.util.List;

interface WeatherFragmentView {

    void setWeathers(List<Entity> weathers);

    void setError();

}
