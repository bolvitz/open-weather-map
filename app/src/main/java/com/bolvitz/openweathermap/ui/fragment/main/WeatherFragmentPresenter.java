package com.bolvitz.openweathermap.ui.fragment.main;

import com.bolvitz.openweathermap.controller.ApiController;
import com.bolvitz.openweathermap.controller.Callback;
import com.bolvitz.openweathermap.model.Entity;
import com.bolvitz.openweathermap.model.MultiEntityResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class WeatherFragmentPresenter {

    private WeatherFragmentView view;
    private ApiController apiController;

    WeatherFragmentPresenter(WeatherFragmentView view, ApiController apiController) {
        this.view = view;
        this.apiController = apiController;
    }

    void getWeathers() {
        apiController.getWeatherByIds(new Callback.MultiEntityListener() {
            @Override
            public void onSuccess(Object object) {
                MultiEntityResponse response = (MultiEntityResponse) object;
                if (response != null) {
                    List<Entity> entities = new ArrayList<>(Arrays.asList(response.list));
                    view.setWeathers(entities);
                }
            }

            @Override
            public void onError() {
                view.setError();
            }
        });

    }
}
