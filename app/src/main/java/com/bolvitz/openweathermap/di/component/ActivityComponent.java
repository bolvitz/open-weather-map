package com.bolvitz.openweathermap.di.component;

import com.bolvitz.openweathermap.di.scope.ActivityScope;
import com.bolvitz.openweathermap.ui.activity.BaseActivity;
import com.bolvitz.openweathermap.ui.activity.details.DetailsActivity;
import com.bolvitz.openweathermap.ui.activity.main.MainActivity;
import com.bolvitz.openweathermap.ui.activity.splash.SplashActivity;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface ActivityComponent extends AppComponent {

    void inject(BaseActivity baseActivity);

    void inject(SplashActivity splashActivity);

    void inject(MainActivity mainActivity);

    void inject(DetailsActivity detailsActivity);

}
