package com.bolvitz.openweathermap.di.module;

import android.content.Context;

import com.bolvitz.openweathermap.OpenWeatherMap;
import com.bolvitz.openweathermap.controller.ApiController;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    final OpenWeatherMap application;

    public AppModule(OpenWeatherMap application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context appContext() {
        return application;
    }

    @Provides
    @Singleton
    public ApiController apiController() {
        return new ApiController(application.getAppComponent());
    }

}
