package com.bolvitz.openweathermap.di.component;

import com.bolvitz.openweathermap.di.scope.ActivityScope;
import com.bolvitz.openweathermap.ui.fragment.BaseFragment;
import com.bolvitz.openweathermap.ui.fragment.main.WeatherFragment;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface FragmentComponent extends AppComponent {

    void inject(BaseFragment baseFragment);

    void inject(WeatherFragment weatherFragment);

}
