package com.bolvitz.openweathermap.di.module;

import com.bolvitz.openweathermap.BuildConfig;
import com.bolvitz.openweathermap.controller.ApiService;
import com.bolvitz.openweathermap.utils.CurlLoggerInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bolvitz.openweathermap.OpenWeatherMap.getContext;

@Module
public class ApiModule {

    private static final String CACHE_CONTROL = "Cache-Control";

    private static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(provideCurlLoggingIntercepter())
                .addNetworkInterceptor(provideCacheInterceptor())
                .cache(provideCache())
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }


    private static Interceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }

    private static Interceptor provideCurlLoggingIntercepter() {
        return new CurlLoggerInterceptor("CURL");
    }

    private static Cache provideCache() {
        if (BuildConfig.DEBUG)
            return new Cache(new File(getContext().getCacheDir(), "http-cache"), 10 * 1024 * 1024); // 10 MB
        else
            return null;
    }

    private static Interceptor provideCacheInterceptor() {
        return chain -> {
            Response response = chain.proceed(chain.request());

            if (BuildConfig.DEBUG) {
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge(0, TimeUnit.MINUTES)
                        .build();

                return response.newBuilder()
                        .header(CACHE_CONTROL, cacheControl.toString())
                        .build();
            } else
                return response = response.newBuilder()
                        .build();
        };
    }


    @Provides
    @Singleton
    public ApiService apiService() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .setPrettyPrinting()
                .create();
        return new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(ApiService.class);
    }

}
