package com.bolvitz.openweathermap.di.component;

import android.content.Context;

import com.bolvitz.openweathermap.controller.ApiController;
import com.bolvitz.openweathermap.controller.ApiService;
import com.bolvitz.openweathermap.di.module.ApiModule;
import com.bolvitz.openweathermap.di.module.AppModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface AppComponent {

    Context appContext();

    ApiController apiController();

    ApiService apiService();

    void inject(ApiController apiController);

}
