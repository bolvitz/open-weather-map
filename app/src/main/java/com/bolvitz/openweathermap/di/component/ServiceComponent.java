package com.bolvitz.openweathermap.di.component;

import com.bolvitz.openweathermap.di.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface ServiceComponent extends AppComponent {

}
