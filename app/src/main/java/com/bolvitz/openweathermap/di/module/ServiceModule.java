package com.bolvitz.openweathermap.di.module;

import android.content.BroadcastReceiver;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

import static com.bolvitz.openweathermap.OpenWeatherMap.getContext;

@Module
public class ServiceModule {

    final BroadcastReceiver service;

    public ServiceModule(BroadcastReceiver service) {
        this.service = service;
    }

    @Provides
    public Context context() {
        return getContext();
    }
}
