package com.bolvitz.openweathermap;

import android.app.Application;
import android.content.Context;

import com.bolvitz.openweathermap.di.component.AppComponent;
import com.bolvitz.openweathermap.di.component.DaggerAppComponent;
import com.bolvitz.openweathermap.di.module.AppModule;
import com.google.gson.Gson;

public class OpenWeatherMap extends Application {

    private static Context context;
    private AppComponent appComponent;

    public static Context getContext() {
        return context;
    }

    public static Gson getGson() {
        return GsonWrapper.instance.obj;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private enum GsonWrapper {
        instance;
        Gson obj = new Gson();
    }
}
