package com.bolvitz.openweathermap.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Weather {

    @SerializedName("id")
    public int id;
    @SerializedName("main")
    public String main;
    @SerializedName("description")
    public String description;
    @SerializedName("icon")
    public String icon;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}