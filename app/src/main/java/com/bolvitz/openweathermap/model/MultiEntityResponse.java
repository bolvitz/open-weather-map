package com.bolvitz.openweathermap.model;

import com.google.gson.Gson;

public class MultiEntityResponse {

    public int cnt;
    public Entity[] list;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}