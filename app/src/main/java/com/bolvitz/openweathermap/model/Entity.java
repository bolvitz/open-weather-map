package com.bolvitz.openweathermap.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Entity implements BaseModel {
    @SerializedName("id")
    public int id;
    @SerializedName("visibility")
    public int visibility;
    @SerializedName("name")
    public String name;
    @SerializedName("weather")
    public Weather[] weather;
    @SerializedName("main")
    public Main main;
    @SerializedName("sys")
    public Sys sys;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}