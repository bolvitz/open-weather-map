package com.bolvitz.openweathermap.model;

import com.google.gson.Gson;

public class Main {

    public float temp;
    public String pressure;
    public String humidity;
    public float temp_min;
    public float temp_max;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}