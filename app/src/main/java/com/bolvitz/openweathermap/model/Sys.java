package com.bolvitz.openweathermap.model;

import com.google.gson.Gson;

public class Sys {

    public String country;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}