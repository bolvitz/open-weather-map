package com.bolvitz.openweathermap.utils.sort;


import com.bolvitz.openweathermap.model.Weather;

import java.util.Comparator;

public class EntityCompare implements Comparator<Weather> {

    public EntityCompare() {
    }

    @Override
    public int compare(Weather lhs, Weather rhs) {
        return lhs.main.compareToIgnoreCase(rhs.main);
    }

    @Override
    public boolean equals(Object object) {
        return false;
    }
}
