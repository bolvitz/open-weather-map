package com.bolvitz.openweathermap.utils;

import com.bolvitz.openweathermap.BuildConfig;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;


public class CurlLoggerInterceptor implements Interceptor {
    private final Charset UTF8 = Charset.forName("UTF-8");
    private StringBuffer stringBuffer;
    private String tag = null;

    /**
     * Set logcat tag for curl lib to make it ease to filter curl logs only.
     *
     * @param tag
     */
    public CurlLoggerInterceptor(String tag) {
        this.tag = tag;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        this.stringBuffer = new StringBuffer("");
        // add cURL command
        this.stringBuffer.append("cURL -g ");
        this.stringBuffer.append("-X ");
        // add method
        this.stringBuffer.append(request.method().toUpperCase()).append(" ");
        // adding headers
        for (String headerName : request.headers().names()) {
            addHeader(headerName, request.headers().get(headerName));
        }

        // adding request body
        RequestBody requestBody = request.body();
        if (request.body() != null) {
            Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            MediaType contentType = requestBody.contentType();
            if (contentType != null) {
                addHeader("Content-Type", request.body().contentType().toString());
                Charset charset = contentType.charset(UTF8);
                this.stringBuffer.append(" -d '").append(buffer.readString(charset)).append("'");
            }
        }

        // add request URL
        this.stringBuffer.append(" \"").append(request.url().toString()).append("\"");
        this.stringBuffer.append(" -L");

        if (BuildConfig.DEBUG) {
            CurlPrinter.print(tag, request.url().toString(), this.stringBuffer.toString());
        }
        return chain.proceed(request);
    }

    private void addHeader(String headerName, String headerValue) {
        this.stringBuffer.append("-H " + "\"").append(headerName).append(": ").append(headerValue).append("\" ");
    }
}
