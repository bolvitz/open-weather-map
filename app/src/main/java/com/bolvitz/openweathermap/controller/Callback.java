package com.bolvitz.openweathermap.controller;

import com.bolvitz.openweathermap.model.Entity;

public class Callback {

    public interface SingleEntityListener {

        void onSuccess(Object object);

        void onError();
    }

    public interface MultiEntityListener {

        void onSuccess(Object object);

        void onError();
    }

    public interface OnItemClick {

        void onItemClick(Entity entity, int position);

    }

}

