package com.bolvitz.openweathermap.controller;

import com.bolvitz.openweathermap.model.MultiEntityResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("group?")
    Call<MultiEntityResponse> getWeathers(@Query("id") String cityId, @Query("appid") String key);

}