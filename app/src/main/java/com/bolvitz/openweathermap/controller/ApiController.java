package com.bolvitz.openweathermap.controller;

import android.support.annotation.NonNull;

import com.bolvitz.openweathermap.di.component.AppComponent;
import com.bolvitz.openweathermap.model.MultiEntityResponse;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

public class ApiController {

    @Inject
    transient ApiService apiService;

    public ApiController(AppComponent appComponent) {
        appComponent.inject(this);
    }

    public void getWeatherByIds(Callback.MultiEntityListener listener) {
        final Call<MultiEntityResponse> getUpcomingReservation = apiService.getWeathers("2643743,4548393,5391959", "e1c8cd19a69b6c2fb4c586792cc33fd0");
        getUpcomingReservation.enqueue(new retrofit2.Callback<MultiEntityResponse>() {
            @Override
            public void onResponse(Call<MultiEntityResponse> call, @NonNull Response<MultiEntityResponse> response) {
                if (response.isSuccessful()) {
                    if (listener != null)
                        listener.onSuccess(response.body());
                } else {
                    if (listener != null)
                        listener.onError();
                }
            }

            @Override
            public void onFailure(Call<MultiEntityResponse> call, Throwable t) {
                if (listener != null)
                    listener.onError();
            }
        });
    }

    public void getWeatherById(int id, Callback.MultiEntityListener listener) {
        final Call<MultiEntityResponse> getUpcomingReservation = apiService.getWeathers(String.valueOf(id), "e1c8cd19a69b6c2fb4c586792cc33fd0");
        getUpcomingReservation.enqueue(new retrofit2.Callback<MultiEntityResponse>() {
            @Override
            public void onResponse(Call<MultiEntityResponse> call, @NonNull Response<MultiEntityResponse> response) {
                if (response.isSuccessful()) {
                    if (listener != null)
                        listener.onSuccess(response.body());
                } else {
                    if (listener != null)
                        listener.onError();
                }
            }

            @Override
            public void onFailure(Call<MultiEntityResponse> call, Throwable t) {
                if (listener != null)
                    listener.onError();
            }
        });
    }

}
